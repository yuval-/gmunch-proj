import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { ProductsChannel, StoresChannel } from '../interfaces/channel.interface';
import { Store } from '../interfaces/store.interface';
import { StoreProduct } from '../interfaces/store.interface';

interface AppDataResponse {
  ok: Boolean;
  data: {
    channels: Array<ProductsChannel & StoresChannel>
  }
}
interface StoreNameResponse {
  ok: Boolean;
  data: {
    store: Store;
    products: StoreProduct[];
  }
}
@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  public getAppData(): Observable<AppDataResponse> {
    return this.http.get<AppDataResponse>('https://api.gmunch.com/gmunch/home')
  }

  public getStoreByName(storeName: string): Observable<StoreNameResponse> {
    return this.http.get<StoreNameResponse>(`https://api.gmunch.com/gmunch/store?name=${storeName}`)
  }
}
