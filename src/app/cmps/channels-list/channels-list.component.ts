import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { StoresChannel } from '../../interfaces/channel.interface';
import { ProductsChannel } from '../../interfaces/channel.interface';
import { AppService } from '../../services/app.service';
@Component({
  selector: 'channels-list',
  templateUrl: './channels-list.component.html',
  styleUrls: ['./channels-list.component.scss']
})
export class ChannelsListComponent implements OnInit {

  channels: Array<StoresChannel & ProductsChannel> = []
  filterTerm: string = '';

  constructor(private appService: AppService) { }

  ngOnInit(): void {
    this.getAppData()
  }

  public async getAppData() {
    const res = await this.appService.getAppData().toPromise()
    this.channels = res.data.channels;
  }

  public filter() {
    const searchTerm = this.filterTerm.toLocaleLowerCase();
    if (searchTerm !== '') {
      let copiedChannels = _.cloneDeep(this.channels);
      const filteredChannels = copiedChannels.map((channel) => {
        if (channel.stores) {
          channel.stores = channel.stores.filter((store) => {
            return store?.name && store.name.toLocaleLowerCase().includes(searchTerm);
          })
        } else {
          channel.products = channel.products.filter(product => {
            return product?.name && product.name.toLocaleLowerCase().includes(searchTerm);
          })
        }
        return channel;
      });
      return filteredChannels
    }
    else return this.channels
  }

  public setFilter(searchTerm: string) {
    this.filterTerm = searchTerm;
  }

}



