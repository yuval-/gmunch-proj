import { Component, Input } from '@angular/core';
import { Store } from '../../interfaces/store.interface'

@Component({
  selector: 'store-preview',
  templateUrl: './store-preview.component.html',
  styleUrls: ['./store-preview.component.scss']
})
export class StorePreviewComponent {

  @Input() store: Store;

}
