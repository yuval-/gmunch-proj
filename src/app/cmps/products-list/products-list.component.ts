import { Component, OnInit, Input } from '@angular/core';
import { StoreProduct } from '../../interfaces/store.interface';
import { ChannelProduct } from '../../interfaces/store.interface'
@Component({
  selector: 'products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  isListExpanded: boolean;
  btnTxt: string = 'הצג הכל';

  @Input() products: StoreProduct[] | ChannelProduct = [];
  @Input() isInChannel: boolean;

  ngOnInit(): void {
    this.isListExpanded = this.isInChannel ? false : true;
  }

  public toggleExpandList() {
    this.isListExpanded = !this.isListExpanded;
    this.btnTxt = this.isListExpanded ? 'הסתר' : 'הצג הכל';
  }

}
