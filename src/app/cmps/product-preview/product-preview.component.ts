import { Component, Input, OnInit } from '@angular/core';
import { StoreProduct } from '../../interfaces/store.interface';
import { ChannelProduct } from '../../interfaces/store.interface';
@Component({
  selector: 'product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.scss']
})
export class ProductPreviewComponent implements OnInit {

  @Input() product: ChannelProduct | StoreProduct;

  ngOnInit(): void {
    this.getProductPrice()
  }

  public getProductPrice() {
    let priceLen = this.product.price.toString().length;
    let price = this.product.price.toString();
    let newPrice;
    newPrice = [price.slice(0, (priceLen - 2)), '.', price.slice(priceLen - 2)].join('')
    this.product.price = newPrice;
  }

}
