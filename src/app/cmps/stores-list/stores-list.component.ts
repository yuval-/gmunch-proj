import { Component, Input } from '@angular/core';
import { Store } from '../../interfaces/store.interface'
@Component({
  selector: 'stores-list',
  templateUrl: './stores-list.component.html',
  styleUrls: ['./stores-list.component.scss']
})
export class StoresListComponent {

  isListExpanded: boolean = false;
  btnTxt: string = 'הצג הכל';
  @Input() stores: Store[] = [];

  public toggleExpandList() {
    this.isListExpanded = !this.isListExpanded;
    this.btnTxt = this.isListExpanded ? 'הסתר' : 'הצג הכל';
  }

}
