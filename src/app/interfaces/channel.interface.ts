import { Store } from './store.interface'
import { ChannelProduct } from './store.interface'
export interface StoresChannel {
    id: number;
    name: string;
    stores: Store[];
}
export interface ProductsChannel {
    id: number;
    name: string;
    products: ChannelProduct[];
}