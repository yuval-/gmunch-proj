export interface Owner {
    id: number;
    name: string;
    created: Date;
    profile_picture: string;
    payme_seller: boolean;
}

export interface Location {
    id: number;
    description: string;
    country: string;
    city: string;
    street: string;
    number?: string | number;
    lat: number;
    lng: number;
}

export interface DeliveryLocation {
    id: number;
    external_id: string;
    description: string;
    country: string;
    city: string;
    street: string;
    number: string;
    lat: number;
    lng: number;
}

export interface DeliveryTo {
    id: number;
    store_id: number;
    location_id: number;
    radius: number;
    price: number;
    currency_id: number;
    created: Date;
    location: DeliveryLocation;
}

export interface ChannelProduct {
    id: number;
    channel_id: number;
    name: string;
    description: string;
    currency_id: number;
    price: number;
    created: Date;
    cover_photo: string;
    expiration: Date;
    groups: any[];
    distance?: any;
}

export interface StoreProduct {
    id: number;
    channel_id: number;
    name: string;
    description: string;
    currency_id: number;
    price: number;
    created: Date;
    cover_photo: string;
    groups: any[];
}

export interface Rating {
    id: number;
    user_id: number;
    store_id: number;
    rating: number;
    description: string;
    created: Date;
}

export interface Store {
    id: number;
    name: string;
    description: string;
    status: number;
    owner: Owner;
    location: Location;
    delivery_to: DeliveryTo[];
    categories: number[];
    ratings: Rating[];
    products: StoreProduct[];
}
