import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { AppService } from '../../services/app.service';
import { Store } from '../../interfaces/store.interface';
import { StoreProduct } from '../../interfaces/store.interface'
@Component({
  selector: 'store-profile',
  templateUrl: './store-profile.component.html',
  styleUrls: ['./store-profile.component.scss']
})

export class StoreProfileComponent implements OnInit {

  storeName: string
  store: Store
  products: StoreProduct[] = []

  constructor(private route: ActivatedRoute, private appService: AppService) { }

  ngOnInit(): void {
    this.storeName = this.route.snapshot.paramMap.get('name')
    this.getStoreByName(this.storeName);
  }

  public async getStoreByName(name: string) {
    const res = await this.appService.getStoreByName(name).toPromise()
    this.store = res.data.store
    this.products = res.data.products
  }

}
