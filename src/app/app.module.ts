import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { StoresListComponent } from './cmps/stores-list/stores-list.component';
import { StorePreviewComponent } from './cmps/store-preview/store-preview.component';
import { StoreProfileComponent } from './pages/store-profile/store-profile.component';
import { ChannelsListComponent } from './cmps/channels-list/channels-list.component';
import { AppHeaderComponent } from './cmps/app-header/app-header.component';
import { ProductPreviewComponent } from './cmps/product-preview/product-preview.component';
import { ProductsListComponent } from './cmps/products-list/products-list.component';
import { FilterComponent } from './cmps/filter/filter.component';


const routes: Routes = [
  { path: 'home', component: ChannelsListComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'store/:name', component: StoreProfileComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    StoresListComponent,
    StorePreviewComponent,
    ChannelsListComponent,
    AppHeaderComponent,
    ProductPreviewComponent,
    ProductsListComponent,
    StoreProfileComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {

}
